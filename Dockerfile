FROM node:16-alpine

RUN apk update && apk add git

COPY ["package.json", "yarn.lock", "tsconfig.json", "/"]
COPY src/ src/
RUN yarn install
RUN yarn build
RUN mv dist/ devcycle-pr-insights-ci-cd/
RUN chmod a+x /devcycle-pr-insights-ci-cd/index.js
