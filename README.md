## Overview
With this GitLab CI/CD pipeline, information on which [DevCycle](https://devcycle.com/) features have been added or removed in a code change will be shown directly on each Merge Request as a comment.

Note: This is intended to run for all merge requests.

### Example Output

![Example Output](https://gitlab.com/devcycle/devcycle-pr-insights-ci-cd/-/raw/main/example_output.png)

### Access Tokens
This pipeline uses the GitLab API to add comments to a Merge Requests. To do this, it requires a GitLab access token with the `api` scope. GitLab provides different types of access tokens, but we recommend using a [project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) for this pipeline. If your GitLab plan does not support project access tokens, a personal access token can be used instead.

To create a project access token, follow the instuctions outlined in the GitLab [documentation](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#create-a-project-access-token).

To create a personal access token, follow the instuctions outlined in the GitLab [documentation](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token).

The access token should be stored as a GitLab CI/CD variable in your project settings. To do this, navigate to your project settings, select `CI/CD` from the left sidebar, and then select `Variables`. From here, you can add a new variable with the name `GITLAB_ACCESS_TOKEN` and the value of your access token.

### Usage
Create a new `.gitlab-ci.yml` file in your GitLab repository or update the existing one. Add the code_usages stage and paste the following code into a code_usages:

```yaml
pr_insights:
  stage: pr_insights
  image: devcycleinfra/devcycle-pr-insights-gitlab:latest
  script:
    - git fetch origin $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    - git fetch origin $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
    - node /devcycle-pr-insights-ci-cd
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
```

Your DevCycle API credentials and project token are required to update the DevCycle dashboard.

When referencing your API client ID and secret, we recommend using GitLab CI/CD variables to store your credentials securely.
## Environment Variables

Your DevCycle API credentials and project token are required to update the DevCycle dashboard. These should be set as environment variables in your GitLab project settings:

| Variable               | Description |
|------------------------| ----------- |
| `DEVCYCLE_PROJECT_KEY` | Your DevCycle project key, see [Projects](https://app.devcycle.com/r/projects) |
| `DEVCYCLE_CLIENT_ID`        | Your organization's API client ID, see [Organization Settings](https://app.devcycle.com/r/settings) |
| `DEVCYCLE_CLIENT_SECRET`    | Your organization's API client secret, see [Organization Settings](https://app.devcycle.com/r/settings) |

When setting these environment variables, we recommend you protect them to ensure they're only exposed to protected branches or tags and mask them to hide their values in job logs.

## Usage

1. Set the necessary environment variables in your GitLab project settings as described above.
2. Add the provided `.gitlab-ci.yaml` to your project root.
3. Push your changes. The pipeline should run automatically on any merge request.

## Support

For any issues, feedback, or questions, please feel free to open an issue on this repository.
