jest.mock('child_process', () => {
  return {
    execSync: jest.fn().mockResolvedValue('DevCycle Variable Changes'),
  }
})

jest.mock('axios', () => {
  return {
    get: jest.fn((url) => {
      let data: any = { links: { self: { href: '' } } }
      if (url.includes('/notes')) data = []
      return Promise.resolve({ data })
    }),
    post: jest.fn(() => Promise.resolve({ data: {}})),
    put: jest.fn(() => Promise.resolve({ data: {} }))
  }
})

describe('DevCycle PR Insights pipe test',  () => {
  const consoleSpy = jest.spyOn(console, 'warn')
  const originalEnv = process.env

  beforeEach(() => {
    jest.resetModules()
    jest.clearAllMocks()
    process.env = { ...originalEnv }
    process.env.CI_MERGE_REQUEST_IID = '1'
    process.env.CI_MERGE_REQUEST_PROJECT_ID = 'testProject'
    process.env.GITLAB_ACCESS_TOKEN = 'token'
  })

  it(('throws error when missing BITBUCKET_PR_ID'), async () => {
    delete process.env.CI_MERGE_REQUEST_IID
    const { run } = require('./index')

    await expect(run).rejects.toThrowError('Requires a merge request')
  })

  it(('calls the dvc cli with auth details'), async () => {
    process.env.DEVCYCLE_PROJECT_KEY = 'my_project'
    process.env.DEVCYCLE_CLIENT_ID = 'client1'
    process.env.DEVCYCLE_CLIENT_SECRET = 'supersecret'

    const { execSync } = require('child_process')
    const { run } = require('./index')

    await run()
    expect(execSync).toHaveBeenCalledWith(
      expect.stringContaining('--project my_project --client-id client1 --client-secret supersecret')
    )
  })

  it('sends request to create a PR comment', async () => {
    const axios = require('axios')
    const { run } = require('./index')

    await run()
    expect(axios.post).lastCalledWith(
      `https://gitlab.com/api/v4/projects/${process.env.CI_MERGE_REQUEST_PROJECT_ID}/merge_requests/${process.env.CI_MERGE_REQUEST_IID}/notes`,
      expect.objectContaining({}),
      expect.objectContaining({})
    )
  })

  it('sends request to update an existing PR comment', async () => {
    const axios = require('axios')
    const { run } = require('./index')

    axios.get.mockImplementation((url: string) => {
      let data: any = []
      if (url.includes('/notes')) {
        data = [{
          id: '123',
          body: 'DevCycle Variable Changes'
        }]
      }
      return Promise.resolve({ data })
    })

    await run()
    expect(axios.put).lastCalledWith(
        `https://gitlab.com/api/v4/projects/${process.env.CI_MERGE_REQUEST_PROJECT_ID}/merge_requests/${process.env.CI_MERGE_REQUEST_IID}/notes/123`,
      expect.objectContaining({}),
      expect.objectContaining({})
    )
  })

})
