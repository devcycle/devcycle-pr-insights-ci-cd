import { execSync } from 'child_process'
import axios from 'axios'

const MR_ID = process.env.CI_MERGE_REQUEST_IID
const PROJECT_ID = process.env.CI_MERGE_REQUEST_PROJECT_ID
const SOURCE_BRANCH = process.env.CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
const DESTINATION_BRANCH = process.env.CI_MERGE_REQUEST_TARGET_BRANCH_NAME
const PROJECT_PATH = process.env.CI_MERGE_REQUEST_PROJECT_PATH

const ACCESS_TOKEN = process.env.GITLAB_ACCESS_TOKEN

const PROJECT_KEY = process.env.DEVCYCLE_PROJECT_KEY || process.env.DVC_PROJECT_KEY
const CLIENT_ID = process.env.DEVCYCLE_CLIENT_ID || process.env.DVC_CLIENT_ID
const CLIENT_SECRET = process.env.DEVCYCLE_CLIENT_SECRET || process.env.DVC_CLIENT_SECRET

const GITLAB_API = `https://gitlab.com/api/v4/projects/${PROJECT_ID}`
const auth = {
    Authorization: `Bearer ${ACCESS_TOKEN}`
}


async function fetchComments(): Promise<any> {
    console.debug('Fetching comments')
    return axios
        .get(
            `${GITLAB_API}/merge_requests/${MR_ID}/notes`,
            { headers: auth }
        )
        .then(({ data }) => data)
        .catch((err: any) => {
            console.error(`Error fetching comments for merge request ${MR_ID}`)
            throw new Error(err.message)
        })
}

async function createComment(body: string): Promise<any> {
    console.debug('Creating comment')
    console.debug(`Comment body: ${body}`)
    return axios
        .post(
            `${GITLAB_API}/merge_requests/${MR_ID}/notes`,
            { body },
            { headers: auth }
        )
        .catch((err: any) => {
            console.error(`Error creating comment on merge request ${MR_ID}`)
            throw new Error(err.message)
        })
}

async function updateComment(noteId: string, body: string): Promise<any> {
    console.debug('Updating comment')
    return axios
        .put(
            `${GITLAB_API}/merge_requests/${MR_ID}/notes/${noteId}`,
            { body },
            { headers: auth }
        )
        .catch((err: any) => {
            console.error(`Error updating comment ${noteId} on merge request ${MR_ID}`)
            throw new Error(err.message)
        })
}

export async function run() {
    const callerString = 'gitlab.pr_insights'
    if (!MR_ID) {
        throw new Error('Requires a merge request')
    }

    if (!ACCESS_TOKEN) {
        throw new Error('GITLAB_ACCESS_TOKEN required')
    }

    console.debug('Installing @devcycle/cli')
    execSync('npm install --location=global @devcycle/cli@5.14.4')

    const prLink = `https://gitlab.com/${PROJECT_PATH}/-/merge-requests/${MR_ID}`

    const authArgs = PROJECT_KEY && CLIENT_ID && CLIENT_SECRET
        ? ['--project', PROJECT_KEY, '--client-id', CLIENT_ID, '--client-secret', CLIENT_SECRET]
        : []

    const callerArgs = ['--caller', callerString]

    console.debug('Analyzing diff')
    const output = execSync([
        'dvc',
        'diff',
        `origin/${DESTINATION_BRANCH}...origin/${SOURCE_BRANCH}`,
        '--format', 'markdown-no-html',
        '--pr-link', prLink,
        ...authArgs,
        ...callerArgs
    ].join(' ')).toString().replace(/⚠️/g, ':warning:')

    const commentIdentifier = 'DevCycle Variable Changes'

    const existingComments = await fetchComments()

    const commentToUpdate = existingComments?.find((comment: any) => (
        comment?.body.includes(commentIdentifier)
    ))

    const commentBody = `${output} \n\n Last Updated: ${(new Date()).toUTCString()}`
    if (commentToUpdate) {
        updateComment(
            commentToUpdate.id,
            commentBody
        )
    } else {
        createComment(commentBody)
    }
}

if (process.env.NODE_ENV !== 'test') run()
